libtest-cukes-perl (0.11-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:59:30 +0100

libtest-cukes-perl (0.11-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ intrigeri ]
  * New upstream version 0.11 (Closes: #845808)
  * autopkgtest: drop obsolete exception
  * Update dependencies: libany-moose-perl → libmoose-perl
  * Update upstream copyright information
  * Bump debhelper compatibility level to 13
  * Declare compliance with Debian Policy 4.5.0
  * debian/upstream/metadata: drop duplicate fields, add bug and repository URLs
  * Add build-dependency on libmodule-build-tiny-perl
  * Un-version dependencies on libtry-tiny-perl (satisfied in oldstable)
  * Annotate test-only build-dependencies with <!nocheck>
  * Make dependency on libtest-simple-perl explicit
  * Declare that debian/rules does not need (fake)root
  * debian/rules: drop empty man pages

 -- intrigeri <intrigeri@debian.org>  Tue, 19 May 2020 05:26:05 +0000

libtest-cukes-perl (0.10-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Christine Spang from Uploaders. Thanks for your work!
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Niko Tyni ]
  * Fix autopkgtest failures due to Any::Moose deprecation (See #845808)
  * Update to Standards-Version 3.9.8
  * Add myself to Uploaders

 -- Niko Tyni <ntyni@debian.org>  Sun, 04 Dec 2016 22:13:46 +0200

libtest-cukes-perl (0.10-2) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Mark package as autopkg-testable

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 22 Dec 2015 22:35:33 -0200

libtest-cukes-perl (0.10-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Refresh copyright information
  * Use new 3.0 (quilt) source format
  * Standards-Version 3.9.1 (no changes)
  * Drop version deps satisfied by oldstable

  [ Ansgar Burchardt ]
  * Test::Simple 0.92 (for Test::Builder::Module 0.80) is also a runtime
    dependency.

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 25 Dec 2010 20:02:31 -0500

libtest-cukes-perl (0.09-1) unstable; urgency=low

  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Add myself to Uploaders and Copyright
  * Depend on Test::Simple 0.92 for Test::Builder::Module 0.80
  * No longer installs examples/
  * Rewrite control description
  * Update copyright to new DEP5 format

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 13 Mar 2010 16:08:44 -0500

libtest-cukes-perl (0.08-2) unstable; urgency=low

  [ Jonathan Yu ]
  * Changed libtest-simple-perl requirement to >= 0.82. (Closes: #544240)

 -- Christine Spang <christine@debian.org>  Sat, 29 Aug 2009 22:42:34 -0400

libtest-cukes-perl (0.08-1) unstable; urgency=low

  * Initial Release. (Closes: #543584)

 -- Christine Spang <christine@debian.org>  Tue, 25 Aug 2009 18:53:45 -0400
